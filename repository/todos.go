package repository

import (
	"back-end/models"
)

func GetTodoRepository() ([]models.Todo, error) {

	const q = `SELECT id,text FROM test.todos ORDER BY id DESC LIMIT 100`

	rows, err := models.Db.Query(q)
	if err != nil {
		return nil, err
	}

	results := make([]models.Todo, 0)

	for rows.Next() {
		var id string
		var text string
		// scanning the data from the returned rows
		err = rows.Scan(&id, &text)
		if err != nil {
			return nil, err
		}
		// creating a new result
		results = append(results, models.Todo{id, text})
	}

	return results, nil
}

func AddTodoRepository(todo models.Todo) error {
	stmt, err := models.Db.Prepare("INSERT INTO test.todos(text) VALUES (?)")
	_, err = stmt.Exec(todo.Text)

	return err
}
