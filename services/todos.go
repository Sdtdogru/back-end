package services

import (
	"back-end/models"
	"back-end/repository"
	"github.com/gin-gonic/gin"
	"net/http"
)

func GetTodoController(context *gin.Context) {
	results, err := repository.GetTodoRepository()
	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"status": "internal error: "})
		return
	}

	context.JSON(http.StatusOK, results)

}

func AddTodoController(context *gin.Context) {
	var t models.Todo
	// reading the request's body & parsing the json
	if context.Bind(&t) == nil {
		if err := repository.AddTodoRepository(t); err != nil {
			context.JSON(http.StatusInternalServerError, gin.H{"status": "internal error: " + err.Error()})
			return
		}
		context.JSON(http.StatusOK, gin.H{"status": "ok"})
		return
	}
	// if binding was not successful, return an error
	context.JSON(http.StatusUnprocessableEntity, gin.H{"status": "invalid body"})
}
