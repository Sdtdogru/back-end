package main

import (
	"fmt"
	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"
	"path/filepath"
	"testing"
)

func TestProvider(t *testing.T) {

	// Create Pact connecting to local Daemon
	pact := &dsl.Pact{
		Provider: "Provider",
	}

	// Start provider API in the background
	go main()
	// Verify the Provider using the locally saved Pact Files
	pact.VerifyProvider(t, types.VerifyRequest{
		ProviderBaseURL: "http://localhost:3000",
		PactURLs:        []string{filepath.ToSlash(fmt.Sprintf("back-end/pact/myconsumer-myprovider.json"))},
		StateHandlers: types.StateHandlers{
			// Setup any state required by the test
			// in this case, we ensure there is a "user" in the system
			"Todo foo exists": func() error {
				//"text" = "denem"
				return nil
			},
		},
	})
}
