package models

type Todo struct {
	Id   string `json:"id"`
	Text string `json:"text" binding:"required"`
}
