package models

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"log"
)

var Db *sql.DB

func ConnectDatabase() error {
	var err error
	Db, err = sql.Open("mysql", "root:root@tcp(127.0.0.1:3306)/test")
	if err != nil {
		panic("Failed to connect to database!")
	}
	//Db.Close()
	if err != nil {
		log.Println("failed to run migrations", err.Error())
		return nil
	}
	return nil
}
