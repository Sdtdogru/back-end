package main

import (
	"back-end/models"
	"back-end/services"
	"github.com/gin-gonic/gin"
	"log"
)

func main() {
	//var err error
	r := gin.Default()

	// Connect to database
	models.ConnectDatabase()

	r.GET("/todo", services.GetTodoController)
	r.POST("/todo", services.AddTodoController)

	log.Println("running..")
	r.Run(":3000")
}
