# back-end

## mySQL database connection </br> 

*database bağlantısını dosya proje içerisnide bulunan models klosörünün altında bulunan setup.go içerisinde gerçekleştirdim.*
</br>

![](./images/contectdatabase.JPG)
</br>

## Main.go kök dosyası </br> 

*main.go içerisinde gin kütüpanesinden yararlanarak bir web servis sunucusu ayağa kaldırdım ve burda get ve post rest servislerinden yararlandım*
</br>

![](./images/main.JPG)
</br>

## Servis katmanı</br> 

*main.go rest servis methodlarını controller klasörünün içerisinde bulunan todo.go dosyasında yer almaktadır burdaki temel amaç çağrıda bulunan mapping isteğini veri tabanından çekmek için ara bir katman olarak oluşturuludu *
</br>

![](./images/controllerservices.JPG)
</br>

## Repository katmanı </br> 

*Bağlanmış olduğumuz veri tabanı üzeinde query işlemlerini bu kantmanda gerçekleştirmkteyiz dosyamız repository altında bulunan todo.go dosyasında bulunmaktadır.  *
</br>

![](./images/getrepository.JPG)
</br>
![](./images/addrepository.JPG)
</br>

## Postman test 
</br> 

* Olışturmuş olduğumuz servisleri  http://localhost:3000/todo url üzerinden postman da testleri aşağıdaki gibidir *
</br>
### GET 

![](./images/getrest.JPG)
</br>

### POST 

![](./images/addrest.JPG)
</br>

## POST main_test.go test denemesi başarlı bir şekilde gözlemlenmiştir.
